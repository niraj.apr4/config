(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes '(whiteboard))
 '(custom-safe-themes
   '("4c7228157ba3a48c288ad8ef83c490b94cb29ef01236205e360c2c4db200bb18" "ee77d69f78a1a17dcd141a58367fb5534dfdb07e94924a10a03c54190bb6a0ef" default))
 '(org-agenda-files
   '("~/tasks/home.org" "/home/niraj/tasks/study.org" "/home/niraj/tasks/others.org"))
 '(package-selected-packages
   '(org-modern aas auctex evil vterm yasnippet which-key vertico pdf-tools orderless nord-theme magit laas general evil-surround evil-collection ef-themes doom-themes denote cdlatex))
 '(safe-local-variable-values
   '((denote-known-keywords "ComputerProgram" "ProgrammingLanguage")
     (denote-known-keywords "Tifr" "GATE" "JAM")
     (denote-known-keywords "MIT1801" "Differentiation" "Integration")
     (denote-known-keywords "MIT601x" "Python" "Julia")
     (denote-known-keywords "MIT6431x")
     (denote-infer-keywords)
     (denote-known-keywords "MIT1802" "Differentiation" "Integration")))
 '(tool-bar-mode nil))
